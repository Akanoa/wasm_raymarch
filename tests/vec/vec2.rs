#[cfg(test)]
mod tests {
    use wasm_raymarch::vec::vec2::Vec2;
    use wasm_raymarch::vec::traits::{Normalise, Length};

    #[test]
    fn should_normalize_vec2() {
        let ab = Vec2::new(10.2,10.4);
        println!("{}", ab);
        println!("{}", ab.normalise().length());
        //assert_eq!(ab.normalise())
    }
}