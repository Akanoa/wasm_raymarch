pub mod vec;
pub mod raymarcher;
pub mod scene;

use wasm_bindgen::prelude::*;
use crate::vec::vec2::{Vec2};
use crate::vec::traits::{VecOrFloat, Pow};
use crate::vec::vec3::Vec3;
use crate::vec::maths::{normalise, cross, mix, max};
use crate::vec::vec4::Vec4;

use crate::raymarcher::{raymarch, MAX_DISTANCE};
use crate::scene::{normal, lighting};

extern crate web_sys;

#[wasm_bindgen]
#[derive(Clone, Copy)]
pub struct Uniform {
    resolution : Vec2<f64>,
    pub time: f64
}

#[wasm_bindgen]
impl Uniform {
    pub fn new() -> Uniform {
        Uniform {
            time: 0. as f64,
            resolution: Vec2::new(BUFFER_WIDTH_SIZE as f64 , BUFFER_HEIGHT_SIZE as f64)
        }
    }

    pub fn set_time(&mut self, t: f64) {
        self.time = t;
    }
}

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

const DEG2RAD : f64 = 0.01745329251;

const BUFFER_WIDTH_SIZE: usize = 640;
const BUFFER_HEIGHT_SIZE: usize = 360;


/// w x h pixels but with 4 components per pixels (r, g, b, a)
const OUTPUT_BUFFER_SIZE: usize = BUFFER_WIDTH_SIZE * BUFFER_HEIGHT_SIZE * 4;
static mut OUTPUT_BUFFER: [u8; OUTPUT_BUFFER_SIZE] = [0; OUTPUT_BUFFER_SIZE];

// Returns the pointer toward our image buffer
#[wasm_bindgen]
pub fn get_buffer_pointer() -> *const u8 {
    let pointer: *const u8;
    unsafe {
        pointer = OUTPUT_BUFFER.as_ptr();
    }

    pointer
}


fn main_frag(frag_coord: Vec2<f64>, uniform: Uniform) -> Vec4<f64> {

    let resolution = uniform.resolution;

    let uv = (frag_coord - 0.5* resolution.xy()) / resolution.y();

    // light
    let k = 4.5;
    let t = 0.5;
    let light_position = Vec3::new(
        (uniform.time*t).cos()*k,
        5.5,
        (uniform.time*t).sin()*k
    );

    let phi = - DEG2RAD* -60.;
    let fov = 1.0;
    let radius = 4.;

    let eye = Vec3::new(
        phi.cos()*radius,
        0.5,
        phi.sin()*radius
    );

    let target = vec3!(0.);

    // vectorial basis
    let vertical = vec3!(0., 1., 0.);
    let forward = normalise(target - eye);
    let side = cross(vertical, forward);
    let up = cross(forward, side);

    let screen_position = eye + fov*forward + uv.x() * side + uv.y() * up;
    let ray_director = normalise(screen_position - eye);

    let hit_point_data = raymarch(eye, ray_director);

    // sky
    let light_sky_color = vec3!(0.08, 0.3, 1.0);
    let dark_sky_color = vec3!(0.5, 0.8, 1.0);

    let mut pixel_color = mix(light_sky_color, dark_sky_color, VecOrFloat::Float((uv.y() + 0.5).powf(1.5) as f64) );

    if hit_point_data.distance < MAX_DISTANCE {
        let point = eye + ray_director * hit_point_data.distance;
        let normal_vector = normal(point);
        let light_intensity = lighting(point, normal_vector, light_position);

        let ambiant_light = vec3!(5. as f64, 0., 10.) * 0.07;
        let ambiant_sky = normal_vector.y() * light_sky_color;

        pixel_color = hit_point_data.material.get_material().xyz() * light_intensity;
        pixel_color = pixel_color * (light_intensity + ambiant_light + ambiant_sky)
    }

    // gamma correction
    pixel_color = pixel_color.pow(0.4545);

    vec4!(pixel_color, 1.)
}

#[wasm_bindgen]
pub fn get_pixels_data(uniform: Uniform) {

    for y  in 0..BUFFER_HEIGHT_SIZE {

        for x in 0..BUFFER_WIDTH_SIZE {

            let pixel_index = (y * BUFFER_WIDTH_SIZE + x) * 4;
            let frag_coord = vec2!(x as f64, (y as f64 - BUFFER_HEIGHT_SIZE as f64).abs());
            let pixel = max(main_frag(frag_coord, uniform) * 255., 0.);

            unsafe {

                OUTPUT_BUFFER[pixel_index] = pixel.r() as u8;
                OUTPUT_BUFFER[pixel_index + 1] = pixel.g() as u8;
                OUTPUT_BUFFER[pixel_index + 2 ] = pixel.b() as u8;
                OUTPUT_BUFFER[pixel_index + 3] = pixel.a() as u8;
            }

        }
    }
}
