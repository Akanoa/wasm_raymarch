use crate::vec::vec3::Vec3;
use crate::raymarcher::{RayMarchingData, raymarch};
use crate::vec::traits::{Length, Normalise};
use crate::vec::vec4::Vec4;
use crate::vec::maths::dot;

use crate::{vec3, vec4};

const EPSILON: f64 = 0.01;

#[derive(Copy, Clone, Debug)]
pub enum Material {
    Material1,
    Material2,
    Floor
}

impl Material {
    pub fn get_material(self) -> Vec4<f64> {
        let k = 0.2;
        match self {
            Material::Material1 => vec4!(0.8, 1.1, 5., 1.) * k,
            Material::Material2 => vec4!(0.3, 0.2, 4., 1.) ,
            Material::Floor => vec4!(0.5, 1., 0., 1.) * k
        }
    }
}


fn sd_sphere(point: Vec3<f64>, radius: f64,  material: Material) -> RayMarchingData {
    RayMarchingData::new(point.length() - radius, material)
}

fn sd_plan(point: Vec3<f64>, height: f64,  material: Material) -> RayMarchingData {
    RayMarchingData::new(point.y() - height, material)
}

pub fn scene(point: Vec3<f64>) -> RayMarchingData {
    let sphere_sd = sd_sphere(point - vec3!(0., 0., 0.5), 1.5,Material::Material2);
    let plan_sd = sd_plan(point, -0.2, Material::Floor);

    sphere_sd.min(plan_sd)
}

pub fn normal(point: Vec3<f64>) -> Vec3<f64> {
    let point_distance = scene(point).distance;
    let delta = vec3!(EPSILON, 0., 0.);

    let dx = scene(point + delta.xyy()).distance - point_distance;
    let dy = scene(point + delta.yxy()).distance - point_distance;
    let dz = scene(point + delta.yyx()).distance - point_distance;

    vec3!(dx, dy, dz).normalise()
}

pub fn lighting(point: Vec3<f64>, normal: Vec3<f64>, light_position: Vec3<f64>) -> f64 {
    let light_distance = light_position - point;
    let light_normal_vector = light_distance.normalise();
    let distance_from_point = raymarch(point + normal * 0.01  , light_normal_vector).distance;

    if distance_from_point < light_distance.length() {
        return 0.0
    }

    (0. as f64).max(dot(light_normal_vector, normal))
}