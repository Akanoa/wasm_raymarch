#[macro_export]
macro_rules! vec4 {
    ($x: expr) => {
        Vec4::new($x, $x, $x, $x)
    };
    ($x: expr, $y: expr, $z: expr, $w:expr) => {
        Vec4::new($x, $y, $z, $w)
    };
    ($v3: expr, $w:expr) => {

        match $v3 {
            Vec3 {..} => Vec4::new($v3.x(), $v3.y(), $v3.z(), $w)
        }
    };
}
