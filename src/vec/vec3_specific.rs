use crate::vec::traits::Cross;
use crate::vec::vec3::Vec3;
use num_traits::Float;

impl<T> Cross<Vec3<T>> for Vec3<T> where T : Float {
    // w = u X v
    fn cross(self, rhs: Vec3<T>) -> Vec3<T> {
        Vec3::new(
            self.y()*rhs.z() - self.z()*rhs.y(),
            self.z()*rhs.x() - self.x()*rhs.z(),
            self.x()*rhs.y() - self.y()*rhs.x()
        )
    }
}

#[macro_export]
macro_rules! vec3 {
    ($x: expr) => {
        Vec3::new($x, $x, $x)
    };
    ($x: expr, $y: expr, $z: expr) => {
        Vec3::new($x, $y, $z)
    };
    ($v2: expr, $z : expr) => {
        match $v2 {
            Vec2 {..} => Vec3::new($v2.x(), $v2.y(), $z)
        }
    };
}
