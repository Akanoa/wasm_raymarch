#[macro_export]
macro_rules! vec2 {
    ($x: expr) => {
        Vec2::new($x, $x)
    };
    ($x: expr, $y: expr) => {
        Vec2::new($x, $y)
    };
}
