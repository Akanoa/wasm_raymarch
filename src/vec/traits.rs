use num_traits::Float;


#[derive(Copy, Clone)]
pub enum VecOrFloat<T: Float, Vector> {
    Float(T),
    Vector(Vector)
}

pub trait Length<T>{
    fn length(self) -> T;
}

pub trait Trigo {
    fn cos(self) -> Self;
    fn sin(self) -> Self;
    fn tan(self) -> Self;
    fn acos(self) -> Self;
    fn asin(self) -> Self;
    fn atan(self) -> Self;
}

pub trait Normalise {
    fn normalise(self) -> Self;
}

pub trait Dot<T, Vector> {
    fn dot(self, rhs: Vector) -> T;
}

pub trait Cross<Vector3> {
    fn cross(self, rhs: Vector3) -> Vector3;
}

pub trait Abs {
    fn abs(self) -> Self;
}

pub trait Mix<T: Float, Vector: Mix<T, Vector>> {
    fn mix(self, rhs: Vector, k: VecOrFloat<T, Vector>) -> Vector;
}

pub trait Pow<T> {
    fn pow(self, k:T) -> Self;
}

pub trait Clamp<T> {
    fn min(self, min_value: T) -> Self;
    fn max(self, max_value: T) -> Self;
}