use crate::vec::traits::{Trigo, Dot, Cross, Normalise, Abs, Mix, Pow, VecOrFloat, Clamp};
use num_traits::Float;

// trigo
pub fn cos<Vector: Trigo>(vec: Vector) -> Vector { vec.cos()}
pub fn sin<Vector: Trigo>(vec: Vector) -> Vector { vec.sin()}
pub fn tan<Vector: Trigo>(vec: Vector) -> Vector { vec.tan()}
pub fn acos<Vector: Trigo>(vec: Vector) -> Vector { vec.acos()}
pub fn asin<Vector: Trigo>(vec: Vector) -> Vector { vec.asin()}
pub fn atan<Vector: Trigo>(vec: Vector) -> Vector { vec.atan()}


// vectorial
pub fn dot<T, Vector: Dot<T, Vector>>(v1: Vector, v2: Vector) -> T {v1.dot(v2)}
pub fn cross<Vector3: Cross<Vector3>>(v1: Vector3, v2: Vector3) -> Vector3 {v1.cross(v2)}
pub fn normalise<Vector: Normalise>(vec: Vector) -> Vector {vec.normalise()}

// arithmetics
pub fn abs<Vector: Abs>(vec: Vector) -> Vector {vec.abs()}
pub fn pow<T, Vector: Pow<T>>(vec: Vector, k: T) -> Vector {vec.pow(k)}

// interpolation
pub fn mix<T: Float, Vector: Mix<T, Vector>>(v1: Vector, v2: Vector, coeff: VecOrFloat<T, Vector>) -> Vector {
    v1.mix(v2, coeff)
}

pub fn min<T: Float, Vector: Clamp<T>>(vec: Vector, min_value: T) -> Vector {
    vec.min(min_value)
}

pub fn max<T: Float, Vector: Clamp<T>>(vec: Vector, max_value: T) -> Vector {
    vec.max(max_value)
}

pub fn clamp<T: Float, Vector: Clamp<T>>(vec: Vector, min_value: T, max_value: T) -> Vector {
    vec.min(min_value).max(max_value)
}