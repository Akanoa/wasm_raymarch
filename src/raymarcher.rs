use crate::vec::vec3::Vec3;
use wasm_bindgen::__rt::core::ops::Neg;
use crate::scene::{Material, scene};

// RayMarcher setting
const MIN_DISTANCE: f64 = 0.001;
pub const MAX_DISTANCE: f64 = 20.;
const MAX_ITERATIONS: u8 = 200;

pub fn raymarch(ray_origin: Vec3<f64>, ray_director:Vec3<f64>) -> RayMarchingData {
    let mut distance = 0.;
    let mut material: Material = Material::Floor;
    for _ in 0..MAX_ITERATIONS {
        let current = ray_origin + ray_director * distance;
        let signed_distance = scene(current);
        distance += signed_distance.distance;
        material = signed_distance.material;
        if signed_distance.distance < MIN_DISTANCE || distance > MAX_DISTANCE {
            break
        }
    }
    RayMarchingData::new(distance, material)
}

#[derive(Debug)]
pub struct RayMarchingData {
    pub distance: f64,
    pub material: Material
}

impl Neg for RayMarchingData {
    type Output = RayMarchingData;

    fn neg(self) -> Self::Output {
        RayMarchingData {
            distance: -self.distance,
            material: self.material
        }
    }
}

impl RayMarchingData {

    pub fn new(distance: f64, material: Material) -> Self {
        RayMarchingData { distance, material }
    }

    pub fn min(self, rhs: RayMarchingData) -> RayMarchingData {
        if self.distance < rhs.distance {self} else {rhs}
    }

    pub fn max(self, rhs: RayMarchingData) -> RayMarchingData {
        if self.distance > rhs.distance {self} else {rhs}
    }
}