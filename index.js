import init, {get_buffer_pointer, get_pixels_data, Uniform} from "./pkg/wasm_raymarch.js";

const runWasm = async () => {
    const raymarch = await init("./pkg/wasm_raymarch_bg.wasm");

    const canvas = document.querySelector('canvas')

    const canvasContext =  canvas.getContext('2d')
    const canvasImageData = canvasContext.createImageData(
        canvas.width,
        canvas.height
    );

    canvasContext.clearRect(0, 0, canvas.width, canvas.height);

    const draw = (time) => {

        let uniform = Uniform.new()
        uniform.set_time(time);
        get_pixels_data(uniform);
        const memoryArray = new Uint8Array(raymarch.memory.buffer);

        const pointer = get_buffer_pointer()
        const imageDataArray = memoryArray.slice(
            pointer,
            pointer + canvas.width * canvas.height * 4
        );

        canvasImageData.data.set(imageDataArray);

        canvasContext.clearRect(0, 0, canvas.width, canvas.height);

        canvasContext.putImageData(canvasImageData, 0, 0);

    }


    const start = Date.now()

    const getElapsed = () => {
        return (Date.now() - start) /1000
    }

    draw(getElapsed());
    setInterval(() => draw(getElapsed()), 10)


}

runWasm();