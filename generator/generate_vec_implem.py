from string import Template

template_struct = Template("""
pub struct Vec$n<T:Float> {
    $components
}
""")

template_new = Template("""
    pub fn new($parameters_T) -> Self {
        Vec$n{$parameters}
    }
""")

template_length = Template('''
impl <T>Length<T> for Vec$n<T> where T: Float {
    fn length(self) -> T  {
        ($content).sqrt()
    }
}
''')

template_copy = Template("""
// copy
impl <T>Clone for Vec$n<T> where T: Float {
    fn clone(&self) -> Self {
        Vec$n::new($content)
    }
}

impl <T>Copy for Vec$n<T> where T: Float  {}
""")

template_display = Template("""
impl <T>Display for Vec$n<T> where T: Display + Float{
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "($formatting)", $content)
    }
}
""")

template_arithmetics = Template("""
// by a scalar
impl <T>Div<T> for Vec$n<T> where T: Float + Into<T> {
    type Output = Vec$n<T>;

    fn div(self, rhs: T) -> Self::Output {
        Vec$n::new($content_div_scalar)
    }
}

impl <T>Mul<T> for Vec$n<T> where T: Float + Into<T> {
    type Output = Vec$n<T>;

    fn mul(self, rhs: T) -> Self::Output {
        Vec$n::new($content_mul_scalar)
    }
}

impl <T>Mul<Vec$n<T>> for f64 where T: Float + Into<f64> {
    type Output = Vec$n<f64>;

    fn mul(self, rhs: Vec$n<T>) -> Self::Output {
        Vec$n::new($content_mul_scalar_2)
    }
}

impl <T>Mul<Vec$n<T>> for f32 where T: Float + Into<f32> {
    type Output = Vec$n<f32>;

    fn mul(self, rhs: Vec$n<T>) -> Self::Output {
        Vec$n::new($content_mul_scalar_2)
    }
}

impl <T>Add<Vec$n<T>> for f64 where T: Float + Into<f64> {
    type Output = Vec$n<f64>;

    fn add(self, rhs: Vec$n<T>) -> Self::Output {
        Vec$n::new($content_add_scalar_2)
    }
}
impl <T>Add<Vec$n<T>> for f32 where T: Float + Into<f32> {
    type Output = Vec$n<f32>;

    fn add(self, rhs: Vec$n<T>) -> Self::Output {
        Vec$n::new($content_add_scalar_2)
    }
}

// component wise
impl <T>Add for Vec$n<T> where T: Float {
    type Output = Vec$n<T>;

    fn add(self, rhs: Self) -> Self::Output {
        Vec$n::new($content_component_wise_add)
    }
}

impl <T>Sub for Vec$n<T> where T: Float {
    type Output = Vec$n<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec$n::new($content_component_wise_sub)
    }
}

impl <T>Div for Vec$n<T> where T: Float {
    type Output = Vec$n<T>;

    fn div(self, rhs: Self) -> Self::Output {
        Vec$n::new($content_component_wise_div)
    }
}

impl <T>Mul for Vec$n<T> where T: Float {
    type Output = Vec$n<T>;

    fn mul(self, rhs: Self) -> Self::Output {
        Vec$n::new($content_component_wise_mul)
    }
}

impl <T> Trigo for Vec$n<T> where T: Float {
    fn cos(self) -> Self {
        Vec$n::new($content_component_wise_cos)
    }
    
    fn sin(self) -> Self {
        Vec$n::new($content_component_wise_sin)
    }
    fn tan(self) -> Self {
        Vec$n::new($content_component_wise_tan)
    }
    
    fn acos(self) -> Self {
        Vec$n::new($content_component_wise_acos)
    }
    
    fn asin(self) -> Self {
        Vec$n::new($content_component_wise_asin)
    }
    
    fn atan(self) -> Self {
        Vec$n::new($content_component_wise_atan)
    }
}

impl <T> Normalise for Vec$n<T> where T: Float {
    // n = AB / ||AB||
    fn normalise(self) -> Self {
        self / self.length()
    }
}

impl <T> Abs for Vec$n<T> where T: Float {
    fn abs(self) -> Self {
        Vec$n::new($content_component_wise_abs)
    }
}

$dot
$mix
$pow
$clamp
""")

template_dot = Template("""
impl <T> Dot<T, Vec$n<T>> for Vec$n<T> where T: Float {
    // d = x . y = $description
    fn dot(self, rhs: Vec$n<T>) -> T {
        $content
    }
}
""")

template_pow = Template("""
impl <T> Pow<T> for Vec$n<T> where T: Float {
    fn pow(self, k: T) -> Vec$n<T> {
        Vec$n::new(
            $content
        )
    }
}
""")

template_clamp = Template("""
impl <T> Clamp<T> for Vec$n<T> where T: Float {

    fn min(self, min_value: T) -> Vec$n<T> {
        Vec$n::new(
            $content_min
        )
    }
    
    fn max(self, max_value: T) -> Vec$n<T> {
        Vec$n::new(
            $content_max
        )
    }
}
""")

template_mix = Template("""
impl <T>Mix<T, Vec$n<T>> for Vec$n<T> where T: Float {
    // m = mix(x, y, a) = (1-a)*x + a*y
    fn mix(self, rhs: Vec$n<T>, k: VecOrFloat<T, Vec$n<T>>) -> Vec$n<T> {
        let one = match T::from(1.)  {
            None => panic!("Can't get float from 1.0"),
            Some(x) => x
        };

        match k {
            VecOrFloat::Float(k) => {

                Vec$n::new(
                    $content_k
                )
            },
            VecOrFloat::Vector(vk) => {
                Vec$n::new(
                    $content_vk
                )
            }
        }
    }
}
""")


def generate_length(components):
    t = Template("self.$x*self.$x")
    a = "+".join([t.substitute({'x': x}) for x in components])
    return template_length.safe_substitute({'content': a, 'n': len(components)})


def generate_copy(components):
    t = Template("self.$x")
    a = ", ".join([t.substitute({'x': x}) for x in components])
    return template_copy.safe_substitute({'n': len(components), 'content': a})


def generate_display(components):
    t = Template("self.$x")
    a = ", ".join([t.substitute({'x': x}) for x in components])
    a2 = ", ".join(["{}" for _x in components])
    return template_display.safe_substitute({'n': len(components), 'content': a, 'formatting': a2})


def generate_arithmetics(components):
    def join(t):
        return ", ".join([t.substitute({'x': x}) for x in components])

    t_div_s = Template("self.$x.into() / rhs.into()")
    t_mul_s = Template("self.$x.into() * rhs.into()")
    t_mul_s2 = Template("self * rhs.$x.into()")
    t_add_s2 = Template("self + rhs.$x.into()")
    t_cpm_add = Template("self.$x + rhs.$x")
    t_cpm_sub = Template("self.$x - rhs.$x")
    t_cpm_div = Template("self.$x / rhs.$x")
    t_cpm_mul = Template("self.$x * rhs.$x")
    t_cpm_cos = Template("self.$x.cos()")
    t_cpm_sin = Template("self.$x.sin()")
    t_cpm_tan = Template("self.$x.tan()")
    t_cpm_acos = Template("self.$x.acos()")
    t_cpm_asin = Template("self.$x.asin()")
    t_cpm_atan = Template("self.$x.atan()")
    t_cpm_abs = Template("self.$x.abs()")

    d = {
        'n': len(components),
        'content_div_scalar': join(t_div_s),
        'content_mul_scalar': join(t_mul_s),
        'content_mul_scalar_2': join(t_mul_s2),
        'content_add_scalar_2': join(t_add_s2),
        'content_component_wise_add': join(t_cpm_add),
        'content_component_wise_sub': join(t_cpm_sub),
        'content_component_wise_div': join(t_cpm_div),
        'content_component_wise_mul': join(t_cpm_mul),
        'content_component_wise_cos': join(t_cpm_cos),
        'content_component_wise_sin': join(t_cpm_sin),
        'content_component_wise_tan': join(t_cpm_tan),
        'content_component_wise_acos': join(t_cpm_acos),
        'content_component_wise_asin': join(t_cpm_asin),
        'content_component_wise_atan': join(t_cpm_atan),
        'content_component_wise_abs': join(t_cpm_abs),
        'dot': generate_dot(components),
        'mix': generate_mix(components),
        'pow': generate_pow(components),
        'clamp': generate_clamp(components),
    }

    return template_arithmetics.safe_substitute(d)


def generate_dot(components):
    description = " + ".join(["%(x)s1%(x)s2" % {'x': x} for x in components])

    t = Template("self.$x * rhs.$x")
    content = " + ".join([t.substitute({'x': x}) for x in components])

    return template_dot.safe_substitute({
        'description': description,
        'content': content,
        'n': len(components)
    })


def generate_mix(components):
    def join(t):
        return ",\n                    ".join([t.substitute({'x': x}) for x in components])

    t_vk = Template("self.$x*(one - vk.$x)+rhs.$x*vk.$x")
    t_k = Template("self.$x*(one - k)+rhs.$x*k")

    d = {
        'n': len(components),
        'content_vk': join(t_vk),
        'content_k': join(t_k),
    }

    return template_mix.substitute(d)


def generate_pow(components):
    def join(t):
        return ",\n            ".join([t.substitute({'x': x}) for x in components])

    t = Template("self.$x.powf(k)")

    d = {
        'n': len(components),
        'content': join(t)
    }

    return template_pow.substitute(d)


def generate_clamp(components):
    def join(t):
        return ",\n            ".join([t.substitute({'x': x}) for x in components])

    t_min = Template("self.$x.min(min_value)")
    t_max = Template("self.$x.max(max_value)")

    d = {
        'n': len(components),
        'content_min': join(t_min),
        'content_max': join(t_max),
    }

    return template_clamp.substitute(d)

def generate_new(components):
    def join(t):
        return ", ".join([t.substitute({'x': x}) for x in components])

    p_t = Template("$x : T")
    p = Template("$x")

    d = {
        'n': len(components),
        'parameters_T': join(p_t),
        'parameters': join(p),
    }

    return template_new.safe_substitute(d)


def generate_struct(components):
    t = Template("$x : T")
    c = ",\n    ".join([t.substitute({'x': x}) for x in components])
    return template_struct.safe_substitute({
        'n': len(components),
        'components': c
    })
