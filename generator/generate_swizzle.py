import itertools
from string import Template
from itertools import combinations


def all_combos(s):
    n = len(s)
    for r in range(1, n + 1):
        for combo in combinations(s, r):
            yield combo


template_vec1 = Template('''
    pub fn $x(self) -> T {
        self.$x
    }
''')

template_vec1_alias = Template('''
    pub fn $x_alias(self) -> T {
        self.$x
    }
''')

template_vecn = Template('''
    pub fn $name(self) -> Vec$n<T> {
        Vec$n::new($content)
    }
''')

template_vecn_alias = Template('''
    pub fn $name_alias(self) -> Vec$n<T> {
        Vec$n::new($content)
    }
''')


def get_components_alias(components, aliases):
    return list(map(lambda c: aliases[c], components))


def generate_swizzle(components, aliases):


    if len(components) == 1:

        s = template_vec1.safe_substitute({'x': components[0]})
        if aliases is None:
            return s


        components_alias = get_components_alias(components, aliases)
        return s + template_vec1_alias.safe_substitute({'x_alias': components_alias[0], 'x': components[0]})







    content = ", ".join([Template("self.$x").substitute({'x': x}) for x in components])
    name = "".join(components)
    d = {
        'n': len(components),
        'content': content,
        'name': name
    }
    s = template_vecn.safe_substitute(d)
    if aliases is None:
        return s

    components_alias = get_components_alias(components, aliases)
    name = "".join(components_alias)
    d_alias = {
        'n': len(components),
        'content': content,
        'name_alias': name
    }
    return s + template_vecn_alias.safe_substitute(d_alias)


def generate_swizzles(x, aliases=None):
    a = [p for p in itertools.product(x, repeat=len(x))]

    s = set()

    for e in a:
        for r in set(all_combos(e)):
            s.add(r)

    y = []

    for e in sorted(s):
        y.append(generate_swizzle(e, aliases))

    return "".join(y)